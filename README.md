# TOTP Authenticator

Generates a six-digit one-time password using [TOTP (RFC 6238)][1].

It gets deployed to [IBM Cloud][2] as a Cloud Foundry app, visit [app URL][3] when it's running.

![](https://bitbucket.org/borisredkin/totp-authenticator/downloads/totp-authenticator-screenshot.png)


Dev requirements:

* JDK 17

* Apache Maven 3.8.1

Building
--------

    mvn clean package

Testing
-------

    mvn spring-boot:run

visit http://localhost:8080


[1]: https://tools.ietf.org/html/rfc6238
[2]: https://cloud.ibm.com
[3]: https://totp-authenticator.eu-gb.mybluemix.net
