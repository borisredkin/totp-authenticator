/*
 * MIT License
 *
 * Copyright (c) 2023 Boris
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.mycompany.totpauthenticator.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

class TotpAuthenticatorTest {

  private TotpAuthenticator generator;

  @BeforeEach
  public void setUp() {
    generator = new TotpAuthenticator();
  }

  @Test
  void testGenerateOtp() {
    String secretKey = "AHXQ2W5P6AGKYVKW";

    String code = generator.generateOtp(secretKey);

    assertIs6DigitsCode(code);
  }

  @Test
  void testInvalidBase32Value() {
    String secretKey = "AHXQ2W5P6AGKYVK";

    Executable when = () -> generator.generateOtp(secretKey);

    assertThrows(IllegalArgumentException.class, when);
  }

  @Test
  void testInvalidKeyMaterialOfSecretKey() {
    String secretKey = "A    ";

    Executable when = () -> generator.generateOtp(secretKey);

    assertThrows(IllegalArgumentException.class, when);
  }

  private void assertIs6DigitsCode(String code) {
    assertEquals(6, code.length());
    assertTrue(code.chars().allMatch(Character::isDigit));
  }

}
